import time
import Adafruit_CharLCD as LCD
# Definition du lcd.
lcd_columns = 16
lcd_rows = 2
# Initialisation du lcd avec les broches
lcd = LCD.Adafruit_CharLCDBackpack(address=0x21)
# activer le retroeclairage
lcd.set_backlight(0)
# Afficher le message
lcd.message('Hello Virginie!')
# pendant 5 s
time.sleep(5.0)
# montrer le curseur
lcd.clear()
lcd.show_cursor(True)
lcd.message('curseur')
time.sleep(5.0)
# curseur clignotant.
lcd.clear()
lcd.blink(True)
lcd.message('curseur c')
time.sleep(5.0)
# Stop .
lcd.show_cursor(False)
lcd.blink(False)
# De gauche a droite.
lcd.clear()
message = 'gauche/droite'
lcd.message(message)
for i in range(lcd_columns-len(message)):
    time.sleep(0.5)
    lcd.move_right()
for i in range(lcd_columns-len(message)):
    time.sleep(0.5)
    lcd.move_left()
#eteind le retroeclairage.
time.sleep(2)
lcd.clear()
lcd.set_backlight(1)
