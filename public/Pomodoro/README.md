Exercices

- [+] compléter les textes à trous ______ dans ce README.md
- [+] terminer le pomodoro
- [+] suivre le nombre de pomodoros effectués
- extraire la machine à état dans un fichier js séparé `pomodoro_state_machine.js`
    - dans le `index.html` actuel, utiliser le `pomodoro_state_machine.js`
- créer un nouveau fichier `index2.html` s'appuyant sur `pomodoro_state_machine.js` et avec un visuel différent juste en texte
- tester la machine à état

Pomodoro:

- [+] faire un compte à rebours de 10 à 0;
- [+] Cercle  ou carrer svg;
- [+] Cercle svg annimée;
- [+] Font;
- [+] Mettre le decompte dans le cercle;
- [+] Transformer les secondes en mm:ss;
- [+] Création du 2eme compte à rebours, qui démarre à la fin du premier(pause);
- [+] Créer les bouttons;
- [+] Donner les fonctions des boutons, work = démarrer et commmencer le timer travail. Quand work = 0 alors stop. 

## Init.
```mermaid
stateDiagram-v2

[*] --> init
init --> running : start
running --> running : tick
running --> paused : pause
paused --> paused : tick
paused --> running : continue
paused --> init : reset
running --> init : reset
```

- [X] La page s'affiche: les bouttons et rond pleins et chrono à 0;
    - état: `init`

```js
to_state('init')
```

```mermaid
stateDiagram-v2
init
```

- [X] Quand le boutons work cliquer: 
    - [X] animation du rond
    - [X] timer à partir de 25--
    - événement déclenché: `start`
    - transition états: `init` -> `running`

```js
sm_on_event('start')
```

```mermaid
stateDiagram-v2
init --> running : start
```
## Running.
- [X] A la réception d'un événement tick on décrémente le compteur et on met à jour l'affichage
- [X] Quand le timer de 25 arrive à 0: 
    - [X] arret du rond animer
    - [X] stop du timer à 0;
    - [X] (+bip sonore)
    - [+] apparition du bouton Break : attente pour commencer le timer de pause
    - événement: `tick`
    - transition états: `running` -> `running`

```js
sm_on_event('tick')
```

```mermaid
stateDiagram-v2
running --> running : tick
```
## Pause.
- [X] Quand boutton break cliquer: 
    - [X] annimation du rond
    - [X] timer à partir de 5 --;
    - événement: `pause`
    - transition états: `running` -> `paused`

```js
sm_on_event('pause')
```

```mermaid
stateDiagram-v2
running --> paused : pause
```

- [X] A la réception d'un événement tick on décrémente le compteur et on met à jour l'affichage
- [+] Quand reste_secondes = 0:
    - [+] bip
    - [+] arret de l'animation
    - [+] stop chono =0 
    - [+] affichage du bouton reset, continu;
    - événement: `tick`
    - transition états: `pause` -> `pause`

```js
sm_on_event('tick')
```

```mermaid
stateDiagram-v2
pause --> pause : tick
pause --> running : continue
```

- [X] Quand reset est cliquer: 
    - [X] arret du timer
    - [X] timer = 0;
    - [X] reste en cet état
    - événement: `init`
    - transition états: `running` -> `init`
    - transition états: `pause` -> `init`

```js
to_state('init')
```

```mermaid
stateDiagram-v2
pause --> init : reset
running --> init : reset
```


