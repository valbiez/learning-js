
function onInitEnter() {
    onInitEnterUpdateDisplay();
}

function onRunningEnter() {
    onRunningEnterUpdateDisplay();
}

function onRunningExit() {
    onRunningExitUpdateDisplay();
}

function onRunningEventTick() {
    onRunningEventTickUpdateDisplay();
}

function onWorkFinished() {
    onWorkFinishedUpdateDisplay();
}

function onPausedEnter() {
    onPausedEnterUpdateDisplay();
}

function onPausedExit() {
    onPausedExitUpdateDisplay();
}

function onPausedEventTick() {
    onPausedEventTickUpdateDisplay();
}

function onPauseFinished() {
    onPauseFinishedUpdateDisplay();
}

function stop_ticks() {
    // Arrête les ticks
    clearInterval(interval);
    interval = undefined;
}
function set_timer_to_zero() {
    // remet à zéro le timer
    reste_secondes = 0;
}

function set_timer_duration(mins) {
    // reste_secondes = mins * 60;
    //reste_secondes = 15;
    show_timer_digits();
}

var reste_secondes = 0;
var current_state = null;
let interval;

var state_machine = { // Machine à état
    'init': {
        'enter': () => {
            // Logique pure
            stop_ticks();
            set_timer_to_zero();

            // Logique écran
            onInitEnter();

        },
        'exit': () => {
        },
        'event': (event_id) => {
            if (event_id === 'start') {
                return 'running';
            }
            return 'init';
        }
    },
    'running': {
        'enter': () => {
            // Logique pure
            set_timer_duration(1500);
            reste_secondes = 1500;
            start_ticks();

            // Logique écran
            onRunningEnter();
        },
        'exit': () => {
            // Logique pure
            stop_ticks();
            // Logique écran
            onRunningExit();
        },
        'event': (event_id) => {
            if (event_id === 'pause') {
                return 'paused';
            }
            if (event_id === 'reset') {
                return 'init';
            }
            if (event_id === 'tick') {

                // Logique pure
                reste_secondes--;

                if (reste_secondes <= 0) {
                    // Logique pure
                    stop_ticks();

                    // Logique écran
                    onWorkFinished()

                }
                // Logique écran
                onRunningEventTick();
            }
            return 'running';
        }
    },
    'paused': {
        'enter': () => {
            // Logique pure
            set_timer_duration(300);
            reste_secondes = 300;
            start_ticks();
            // Logique écran
            onPausedEnter();
        },
        'exit': () => {
            onPausedExit();
        },
        'event': (event_id) => {
            if (event_id === 'continue') {
                return 'running';

            }
            if (event_id === 'reset') {
                return 'init';
            }
            if (event_id === 'tick') {
                // Logique pure
                reste_secondes--;

                if (reste_secondes <= 0) {
                    // Logique pure
                    stop_ticks();
                    // Logique écran
                    onPauseFinished();

                }
                // Logique écran
                onPausedEventTick();
            }
            return 'paused';
        }
    }

}

// Petits outils pour animer une machine à états
function to_state(state) {
    console.log("Current state: " + current_state + "; next: " + state);
    if (state === null) {
    }
    if (current_state !== state) {
        if (current_state !== null) {
            console.log("  exiting state " + current_state);
            state_machine[current_state].exit();
        }
        current_state = state;
        console.log("  entering state " + current_state);
        state_machine[current_state].enter();
    }
}

function sm_on_event(event_id) {
    to_state(state_machine[current_state].event(event_id));
}

const updateSM = () => {
    sm_on_event('tick');
}

function start_ticks() {
    if (interval) {
        // ticks en cours, on n'en rajoute pas
    } else {
        // Lance un événement tick toutes les secondes
        interval = setInterval(updateSM, 1000); // Est ce qu'on le met ailleurs?
    }
}
function initPomodoro() {
    to_state('init');
}