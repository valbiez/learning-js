# Lanceur de Dés

- [ ] version écrite
  - [X] Bouton "lancer" : affiche le résultat du lancer
    - [X] Lancer un dé renvoie un résultat aléatoire entre 1 et 6
    - [X] Bouton
    - [X] Affichage
      - [X] récupère le composant de l'affichage et le mettre dans une variable
    - [X] Mettre à jour l'affichage avec cette valeure
    - [X] Associer le bouton à la fonction de lancer
  - [X] Choix du nombre de faces
    - [X] Avec une liste de sélection
    - [X] modifier la fonction de lancer pour prendre un nombre de face plus général
      - [X] mis à jour les tests
    - [X] récupérer le type de dé de la liste
    - [X] utiliser le type de dé pour la partie affichage

Version avec des images

Selon le type de dé, afficher l'image correspondante

ex. 
Dé 2
Afficher une image de pièce (pile)
Afficher une image de pièce (face)

Dé 6

...


```js
document.querySelector("#TypeDe")

document.querySelector("#TypeDe").value

lancer("20")

```
 