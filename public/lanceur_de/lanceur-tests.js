// assert.equal(a, b)
// assert.equal(a, b, "message")
// assert.true(a)
// assert.true(a, "message")
// assert.false(a)
// assert.false(a, "message")

QUnit.module('lancer', function () {
    QUnit.test('un dé à 6 faces', function (assert) {

        const valeurLancee = lancer()
        // un nombre entre 1 et 6
        console.log("Dé à 6 faces - valeur lancée : ", valeurLancee)
        assert.true(valeurLancee >= 1, "une valeur >= 1")
        assert.true(valeurLancee <= 6, "une valeur <= 6")
    });
});

QUnit.module('lancer', function () {
    QUnit.test('un dé à 2 faces', function (assert) {

        const valeurLancee = lancer(2)
        // un nombre entre 1 et 2
        console.log("Dé à 2 faces - valeur lancée : ", valeurLancee)
        assert.true(valeurLancee >= 1, "une valeur >= 1")
        assert.true(valeurLancee <= 2, "une valeur <= 2")
    });
});

QUnit.module('lancer', function () {
    QUnit.test('un dé à 4 faces', function (assert) {

        const valeurLancee = lancer(4)
        // un nombre entre 1 et 4
        console.log("Dé à 4 faces - valeur lancée : ", valeurLancee)
        assert.true(valeurLancee >= 1, "une valeur >= 1")
        assert.true(valeurLancee <= 4, "une valeur <= 4")
    });
});

QUnit.module('lancer', function () {
    QUnit.test('un dé à 8 faces', function (assert) {

        const valeurLancee = lancer(8)
        // un nombre entre 1 et 8
        console.log("Dé à 8 faces - valeur lancée : ", valeurLancee)
        assert.true(valeurLancee >= 1, "une valeur >= 1")
        assert.true(valeurLancee <= 8, "une valeur <= 8")
    });
});

QUnit.module('lancer', function () {
    QUnit.test('un dé à 10 faces', function (assert) {

        const valeurLancee = lancer(10)
        // un nombre entre 1 et 10
        console.log("Dé à 10 faces - valeur lancée : ", valeurLancee)
        assert.true(valeurLancee >= 1, "une valeur >= 1")
        assert.true(valeurLancee <= 10, "une valeur <= 10")
    });
});

QUnit.module('lancer', function () {
    QUnit.test('un dé à 12 faces', function (assert) {

        const valeurLancee = lancer(12)
        // un nombre entre 1 et 12
        console.log("Dé à 12 faces - valeur lancée : ", valeurLancee)
        assert.true(valeurLancee >= 1, "une valeur >= 1")
        assert.true(valeurLancee <= 12, "une valeur <= 12")
    });
});

QUnit.module('lancer', function () {
    QUnit.test('un dé à 20 faces', function (assert) {

        const valeurLancee = lancer(20)
        // un nombre entre 1 et 20
        console.log("Dé à 20 faces - valeur lancée : ", valeurLancee)
        assert.true(valeurLancee >= 1, "une valeur >= 1")
        assert.true(valeurLancee <= 20, "une valeur <= 20")
    });
});
