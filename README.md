## Voir la page web déployée

https://valbiez.gitlab.io/learning-js/

## Pour exécuter le code localement

### Dans le navigateur

Dans Chrome

Ouvrir le fichier index.html

Ouvrir les outils de développeur (F12)
- Ajouter le dossier "learning-js/public" à l'espace de travail

![image](docs/assets/01_chrome_add_folder_to_workspace.png)

### Dans le terminal (Deno)

Deno est facile à installer et permet d'exécuter du code javascript de façon simple.

```
deno run 01-hello.js
```

Voir [ici](docs/deno_install.md) pour les instructions d'installation

### Dans le terminal (NodeJs)

```
node 01-hello.js
```


