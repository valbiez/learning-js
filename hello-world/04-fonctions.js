// une fonction avec un parametre qui retourne une valeur
function hello(name) {
    return `Hello ${name}!`
}

// une fonction avec un paramètre qui ne retourne pas de valeur
function sayHello(name) {
    console.log(hello(name))
}

// une fonction avec plusieurs paramètres
// temps a une valeur par défaut : "beau"
function helloMeteo(name, temps="beau") {
    return `${hello(name)} il fait ${temps}`
}

// déclarer un tableau d'argument
function multiple(... gens) {
    console.log(`Hello ` , gens)
}
