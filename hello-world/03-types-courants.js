// texte

let texteStandard = "je suis du texte"
let texteStandard2 = 'je suis du "texte"'

let personne = "Virginie"

// concaténation de texte

let saluer = "Hello " + personne
let langage = "Javascript"

let chaineTemplate = `Tu t'appeles ${personne} et tu apprends le ${langage}`

let equivalentConcatenation = "Tu t'appeles " + personne + " et tu apprends le " + langage

// Mettre en majuscule
langage.toUpperCase()

// Pour aller plus loin
// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/String


// nombres
let nombre = 1
let somme = 1 + 1
let nombreDecimal = 6.5
let multiplication = 5 * 5
let fraction = 5 / 5
let soustraction = 10 - 30
let nombreNegatif = -20

// Transformer du texte en nombre entier
let nombreDepuisChaine = parseInt("15")

// parseInt("Marc") -> Not a Number
// > NaN

// dates

// date du jour
let now = new Date()
// Récupérer l'année
let annee = now.getFullYear()

// Exercice pour plus tard : combien de temps avant noel
// Attention les mois sont de 0 à 11 
// ex. décembre = 11
let noel = new Date(2022, 11, 25)

// Pour aller plus loin
// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Date

// bool
// vrai faux

let pile = true
let face = false
let coin = true

// condition avec if
if (coin == pile) {
    console.log("Pile je gagne")
} else {
    console.log("Face je perds")
}

// Ici on vérifie que la variable est définie
if ("Marc") {
    console.log("Marc est vrai")
}

if (1 == 1.0) {
    console.log("1 == 1.0")
}

// vérification forte avec ===
if (1 === 1.0) {
    console.log("1 === 1.0")
}

let testDansVariable = 2022 > 2000
let test2 = 2022 < 2000

if (test2) {
    console.log("2022 < 2000 est vrai")
} else {
    console.log("2022 < 2000 est faux")
}
