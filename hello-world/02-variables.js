
// let permet de définir une variable
let nom_de_variable = "Virginie"

console.log("hello " + nom_de_variable)

// on peut la changer si on veut
nom_de_variable = "Marc"

console.log("hello", nom_de_variable)

// on peut définir des variable qu'on ne peut pas changer
// des constantes

const PI = 3.14456456

// PI = 6.24123165
// ne marchera pas
